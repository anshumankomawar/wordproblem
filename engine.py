import clean_questions
import re

TOKEN_SHAPE = 'SHAPE'
TOKEN_QWORD = 'QWORD'
TOKEN_NUMERIC = 'NUMERIC'
TOKEN_IDENTIFIERS = 'IDENTIFIER'
TOKEN_UNITS = 'UNIT'
DIGITS = '0123456789'

question_words = ["what", "where", "how", "when"]
shape_words = ["square", "rectangle", "circle", "trapezoid", "parallelogram"]
identifiers = ["side length", "length of side", "radius", "circumference", "diameter", "area", "diagonal", "height", "base", "equilateral", "angle"]
units = ["cm", "centimeters", "meters"]
 

class Token:
	def __init__(self, type_, value = None):
		self.type = type_
		self.value = value

	def __repr__(self):
		if self.value: return f'{self.type}:{self.value}'
		return f'{self.type}'

def is_int(n):
	try:
		float_n = float(n)
		int_n = int(float_n)
	except ValueError:
		return False
	else:
		return float_n == int_n

def is_float(n):
	try:
		float_n = float(n)
	except ValueError:
		return False
	else:
		return True

class Lexer:
	def create_tokens(self):
		question_list = clean_questions.clean_questions()
		tokens = []
		place = 0
		for current_question in question_list:
			question = current_question.split()
			temp_tokens = []
			p = re.compile('\d+(\.\d+)?')
			for index in range(0, len(question)):
				if question[index] in question_words:
					temp_tokens.append(Token(TOKEN_QWORD, question[index].upper()))
				elif question[index] in shape_words:
					temp_tokens.append(Token(TOKEN_SHAPE, question[index].upper()))
				elif question[index] in identifiers:
					temp_tokens.append(Token(TOKEN_IDENTIFIERS, question[index].upper()))
				elif question[index] in units:
					temp_tokens.append(Token(TOKEN_UNITS, question[index].upper()))
				elif is_float(question[index]):
					temp_tokens.append(Token(TOKEN_NUMERIC, float(question[index])))
				elif is_int(question[index]):
					temp_tokens.append(Token(TOKEN_NUMERIC, int(question[index])))
			tokens.append((current_question, temp_tokens))
		return tokens
					
lexer = Lexer()
tokens = lexer.create_tokens()
print(tokens)

""" 
Circle: 
	radius
	diameter
	area
	sector angle
	arc length
"""