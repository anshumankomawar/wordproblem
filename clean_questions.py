import re
import string

def clean_questions():
	questions = [
	"What is the area of a circle with radius 2.5 meters",
	"What is the area of a circle with diameter 10 meters", 
	"The length of a side of a square is 25cm find its area",
	"The length of half of a diagonal of a rectangle is 25 inches and the longer side is 20 inches find the area of the rectangle."
	]

	cleaned_questions = []

	for current_question in questions:
		current_question = current_question.lower()
		current_question = re.sub('\[.*?\]', '', current_question) 
		current_question = re.sub('(?<!\d)\.|\.(?!\d)', '', current_question) 
		split_list = current_question.split()
		articles = ['is', 'the', 'of', 'a', 'with']
		index = 0
		for word in split_list: 
			if index != len(split_list) - 1:
				split_list[index] = split_list[index] + ' '
			if word in articles:
				split_list[index] = ''
			index += 1
		temp = ""
		cleaned_questions.append((temp.join(split_list)))

#	for question in cleaned_questions:
#		print(question)
	return cleaned_questions